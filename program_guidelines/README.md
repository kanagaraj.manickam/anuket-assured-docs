# Anuket Assured Program (AAP) Governance and Guidelines

## Table of Contents

* [Executive Summary](executive_summary.md)
* [Purpose of this Document](purpose.md)
* [Program Management & Maintenance](program_management_and_maintenance.md)
* [System Under Compliance Verification Requirements](sut_requirements.md)
* [Badge Application Requirements](application_requirements.md)
* [Escalation Process](escalation_process.md)

Licensed under the CREATIVE COMMONS ATTRIBUTION 4.0 INTERNATIONAL LICENSE. 
Full license text at [https://creativecommons.org/licenses/by/4.0/legalcode](https://creativecommons.org/licenses/by/4.0/legalcode)

Copyright Contributors to the Linux Foundation Compliance & Verification Committee
