# Executive Summary

LF Networking is offering compliance verification through its Anuket Assured
Program (AAP). The AAP verifies products (“Offerings”) that claim compliance to
Anuket, ONAP, and/or other LFN projects.

The LFN AAP is a compliance verification program intended to increase the awareness
and adoption of LF Networking technology by demonstrating the readiness and
availability of commercial products using LFN Projects.

The key objectives and benefits of the LFN AAP are to:

* Help build the market for LFN-based SDN/NFV/automation infrastructure and
applications or other devices designed to interface with that infrastructure
* Promote interoperability between "apps", such as CNFs or VNFs and their
virtual and cloud native infrastructure
* Reduce adoption risks for end-users
* Decrease testing costs by verifying hardware and software platform
interfaces and components

Verified offerings submitted by respective vendors are expected to differentiate
themselves with different features and capabilities but remain compliant by
implementing explicitly defined interfaces, behaviors, and key features.
